from django.shortcuts import render
from django.contrib.auth.models import User
from .models import Blog,Comment,Response
from django.db.models import Count, F


def home(request):
    return render(request,"home.html")


# Blogs of a specific user with like and number of comments:
def firstTask(request):
    users = User.objects.all()
    user_data = {}
    for user in users:
        if user.blog_set.all():
            user_data[user.username] = {}
            for blog in user.blog_set.all():
                user_data[user.username][blog.name] = {}
                user_data[user.username][blog.name]['content'] = blog.content
                comments = blog.comment_set.all().count()
                user_data[user.username][blog.name]['comments'] = comments
                likes = blog.response_set.filter(like_or_not = True).count()
                dislikes = blog.response_set.exclude(like_or_not = True).count()
                user_data[user.username][blog.name]['likes'] = likes
                user_data[user.username][blog.name]['dislikes'] = dislikes
    return render(request,'firstTask.html',{'user_data':user_data})


# List of top 5 Commented Blogs:
def secondTask(request):
    blogs = Blog.objects.all()
    top_5 = blogs.annotate(counter = Count('comment')).order_by('-counter')[:5]
    if top_5:
        return render(request,'second.html',{'top_5_blogs':top_5})


# List of 5 recently liked and disliked blogs:
import datetime
def thirdTask(request):
    three_days = datetime.datetime.now() - datetime.timedelta(days = 3)
    blogs = Blog.objects.all()
    like = 0
    dislike = 0
    likes_5 = {}
    dislikes_5 = {}
    
    for blog in blogs:
        likes = blog.response_set.filter(like_or_not = True).filter(response_date__gte = three_days).count()
        dislikes = blog.response_set.exclude(like_or_not = True).filter(response_date__gte = three_days).count()
        likes_5[blog] = likes
        dislikes_5[blog] = dislikes
    sorted_5_likes = {k: v for k, v in sorted(likes_5.items(), key=lambda item: item[1], reverse = True)}
    top_5_likes = dict(list(sorted_5_likes.items())[:5])

    sorted_5_dislikes = {k: v for k, v in sorted(dislikes_5.items(), key=lambda item: item[1], reverse = True)}
    top_5_dislikes = dict(list(sorted_5_dislikes.items())[:5])
    return render(request,'third_task.html',{'likes_5':top_5_likes, 'dislikes_5': top_5_dislikes})    


# List of Blogs which is not modified after creation:
def forthTask(request):
    not_modified_blogs = {}
    users = User.objects.all()
    for user in users:
        data =  user.blog_set.filter(created_date = F('modified_date'))
        if len(data):
            not_modified_blogs[user] = data      
    return render(request,'forthTask.html',{'data':not_modified_blogs})


# lists of Blogs which a user authored or commented:       
def fifthTask(request):
    blog_list = []
    blogs = Blog.objects.all()
    for blog in blogs:
        author = blog.author
        if blog.comment_set.filter(user = author):
            blog_list.append(blog)
    return render(request,'fifthTask.html',{'data':blog_list})


# My recent 5 liked blogs:    
def sixthTask(request):
    blog_list = []
    reader_responses = Response.objects.filter(user__username = 'reader_user').filter(like_or_not = True).order_by('-response_date')[:5]
    for reader_res in reader_responses:
        blog_list.append(reader_res.blog)
    return render(request,'sixthTask.html',{'data':blog_list})


# My Comment History for secific blog:
def seventhTask(request):
    comments = Comment.objects.filter(user__username = 'reader_user').order_by('-modified_date')
    blog_dict = {}
    comment_blog = []
    # reader_user = User.objects.get(username = 'reader_user')
    # comments = reader_user.comment_set.order_by('-modified_date')
    for comment in comments:
        if comment.blog not in comment_blog:
            comment_blog.append(comment.blog)
            blog_dict[comment.blog] = []
            blog_dict[comment.blog].append(comment)
        else:
            blog_dict[comment.blog].append(comment)
    return render(request,'seventhTask.html',{'data':blog_dict})


def eighthTask(request):
    blog_dict = {}
    comment_author_list = []
    commented_blog_list = []
    # reader_user = User.objects.get(username = 'reader_user')
    # comments = reader_user.comment_set.order_by('-modified_date')
    comments = Comment.objects.filter(user__username = 'reader_user').order_by('-modified_date')
    
    for comment in comments:
        if comment.blog.author not in comment_author_list:
            comment_author_list.append(comment.blog.author)
            blog_dict[comment.blog.author] = {}         
            if comment.blog not in commented_blog_list:
                commented_blog_list.append(comment.blog)
                blog_dict[comment.blog.author][comment.blog] = []
                blog_dict[comment.blog.author][comment.blog].append(comment)
            else:
                blog_dict[comment.blog.author][comment.blog].append(comment)
        else:
            if comment.blog not in commented_blog_list:
                commented_blog_list.append(comment.blog)
                blog_dict[comment.blog.author][comment.blog] = []
                blog_dict[comment.blog.author][comment.blog].append(comment)
            else:
                blog_dict[comment.blog.author][comment.blog].append(comment)
    return render(request,'eighthTask.html',{'data':blog_dict})   