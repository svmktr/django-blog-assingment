from django.urls import path, include
from . import views

urlpatterns = [
    path('',views.home, name = "home"),
    path('firsttask/',views.firstTask,name="firsttask"),
    path('secondtask/',views.secondTask,name="secondtask"),
    path('thirdtask/',views.thirdTask,name="thirdtask"),
    path('forthtask/',views.forthTask,name="forthtask"),
    path('fifthtask/',views.fifthTask,name="fifthtask"),
    path('sixthtask/',views.sixthTask,name="sixthtask"),
    path('seventhtask/',views.seventhTask,name="seventhtask"),
    path('eighthtask/',views.eighthTask,name="eighthtask"),
]